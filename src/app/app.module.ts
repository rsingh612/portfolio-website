import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from "./app-routing.module";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {AppComponent} from "./app.component";
import {MainComponent} from "./main/main.component";
import {MediaMatcher} from "@angular/cdk/layout";
import {HomeComponent} from "./home/home.component";
import {FooterComponent} from "./footer/footer.component";
import {ExperienceComponent} from "./experience/experience.component";
import {EducationComponent} from "./education/education.component";
import {AngularFireModule} from "angularfire2";
import {FireBaseConfig} from "../environments/firebase.config";
import {AngularFirestoreModule} from "angularfire2/firestore";
import {AngularFireAuthModule} from "angularfire2/auth";
import {AngularFireDatabaseModule} from "angularfire2/database";
import {WorkHistoryService} from "./services/work-history.service";
import {EducationService} from "./services/education.service";
import {MatCardModule} from "@angular/material/card";
import {MatDividerModule} from "@angular/material/divider";
import {MatChipsModule} from "@angular/material/chips";
import {AboutService} from "./services/about.service";
import {MatExpansionModule} from "@angular/material/expansion";
import {SkillsComponent} from "./skills/skills.component";
import {SkillsService} from "./services/skills.service";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        HomeComponent,
        FooterComponent,
        ExperienceComponent,
        EducationComponent,
        SkillsComponent
    ],
    imports: [
        AppRoutingModule,
        AngularFireModule.initializeApp(FireBaseConfig.firebase),
        AngularFirestoreModule,
        AngularFireAuthModule,
        AngularFireDatabaseModule,
        BrowserModule,
        HttpClientModule,
        NoopAnimationsModule,
        MatCardModule,
        MatDividerModule,
        MatChipsModule,
        MatExpansionModule,
        MatProgressSpinnerModule,
        RouterModule.forRoot([
            {
                path: "",
                component: MainComponent
            },
            {
                path: "**",
                component: MainComponent
            }
        ])
    ],
    providers: [
        MediaMatcher,
        EducationService,
        WorkHistoryService,
        AboutService,
        SkillsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
