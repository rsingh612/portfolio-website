import {Component, OnInit} from "@angular/core";
import {WorkHistoryService} from "../services/work-history.service";

@Component({
    selector: "app-experience",
    templateUrl: "./experience.component.html",
    styleUrls: ["./experience.component.scss"]
})
export class ExperienceComponent implements OnInit {
    private _loaded = false;
    get loaded(): boolean {
        return this._loaded;
    }
    public workHistory: any[] = null;
    constructor(private workHistoryService: WorkHistoryService) {
    }

    ngOnInit() {
        this.workHistoryService.getWorkHistory().subscribe((value) => {
            this.workHistory = value;
            this.workHistory = this.workHistory.reverse();
            this._loaded = true;
        });
    }
}
