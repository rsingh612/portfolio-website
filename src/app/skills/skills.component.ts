import {Component, OnInit} from "@angular/core";
import {SkillsService} from "../services/skills.service";

@Component({
    selector: "app-skills",
    templateUrl: "./skills.component.html",
    styleUrls: ["./skills.component.scss"]
})
export class SkillsComponent implements OnInit {
    private _loaded = false;
    get loaded(): boolean {
        return this._loaded;
    }
    public skills: ISkills = null;

    constructor(private skillsService: SkillsService) {
    }

    ngOnInit() {
        this.skills = {languages: null, frameworks: null, os: null, tools: null};
        this.skillsService.getSkills().subscribe((value) => {
            this.skills.languages = this.arraySort(value[1]);
            this.skills.frameworks = this.arraySort(value[0]);
            this.skills.os = this.arraySort(value[2]);
            this.skills.tools = this.arraySort(value[3]);
            this._loaded = true;
        });
    }

    public arraySort(value: any[]): any[] {
        return value.sort((val1, val2) => {
            if (val1 < val2) {
                return -1;
            } else if (val1 > val2) {
                return 1;
            } else {
                return 0;
            }
        });
    }
}

interface ISkills {
    languages: any[];
    frameworks: any[];
    os: any[];
    tools: any[];
}
