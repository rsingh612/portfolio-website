import {Component, OnInit} from "@angular/core";
import {AboutService} from "../services/about.service";
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";

@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
    private _loaded = false;
    get loaded(): boolean {
        return this._loaded;
    }

    private _sanitedAboutMessage: SafeHtml = null;
    get sanitedAboutMessage(): SafeHtml {
        return this._sanitedAboutMessage;
    }

    public about: any[] = null;

    constructor(private aboutService: AboutService, private _sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        this.aboutService.getAbout().subscribe((value) => {
            let aboutMessage = "";
            value.forEach((item) => {
                aboutMessage += item;
            });
            this._sanitedAboutMessage = this._sanitizer.bypassSecurityTrustHtml(aboutMessage);
            this._loaded = true;
        });
    }
}
