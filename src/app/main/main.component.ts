import {Component, ElementRef, HostListener, OnInit} from "@angular/core";


@Component({
    selector: "app-main",
    templateUrl: "./main.component.html",
    styleUrls: ["./main.component.scss"],
})

export class MainComponent implements OnInit {
    private _navigation: ISideNavItems[] = null;
    public screenWidth: any = null;

    get navigation(): ISideNavItems[] {
        return this._navigation;
    }

    set navigation(value: ISideNavItems[]) {
        this._navigation = value;
    }

    constructor(private el: ElementRef) {
    }

    @HostListener("window:resize") onResize() {
        this.screenWidth = window.innerWidth;
    }

    ngOnInit() {
        this.screenWidth = window.innerWidth;
        this.navigation = [
            {name: "About", route: "about"},
            {name: "Experience", route: "experience"},
            {name: "Education",  route: "education"},
            {name: "Skills",  route: "skills"},
        ];
    }
}

interface ISideNavItems {
    name: string;
    route: string;
}
