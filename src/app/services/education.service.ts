import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database";
import {Observable} from "rxjs/Observable";

@Injectable()
export class EducationService {
    private basePath = "/Education";

    constructor(private db: AngularFireDatabase) {
    }

    getEducation(): Observable<any[]> {
        return this.db.list(this.basePath).valueChanges();
    }
}
