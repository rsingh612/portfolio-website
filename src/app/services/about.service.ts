import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AboutService {
    private basePath = "/about";

    constructor(private db: AngularFireDatabase) {
    }

    getAbout(): Observable<any[]> {
        return this.db.list(this.basePath).valueChanges();
    }
}
