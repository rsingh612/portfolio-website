import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database";
import {Observable} from "rxjs/Observable";

@Injectable()
export class SkillsService {
    private basePath = "/skills";

    constructor(private db: AngularFireDatabase) {
    }

    getSkills(): Observable<any[]> {
        return this.db.list(this.basePath).valueChanges();
    }
}
