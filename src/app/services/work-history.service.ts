import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database";
import {Observable} from "rxjs/Observable";

@Injectable()
export class WorkHistoryService {
    private basePath = "/workHistory";

    constructor(private db: AngularFireDatabase) {
    }

    getWorkHistory(): Observable<any[]> {
        return this.db.list(this.basePath).valueChanges();
    }
}