import {Component, OnInit} from "@angular/core";
import {EducationService} from "../services/education.service";

@Component({
    selector: "app-education",
    templateUrl: "./education.component.html",
    styleUrls: ["./education.component.scss"]
})
export class EducationComponent implements OnInit {
    private _loaded = false;
    get loaded(): boolean {
        return this._loaded;
    }
    public education: any[] = null;
    constructor(private educationService: EducationService) {
    }

    ngOnInit() {
        this.educationService.getEducation().subscribe((value) => {
            this.education = value;
            this.education = this.education.reverse();
            this._loaded = true;
        });
    }
}
